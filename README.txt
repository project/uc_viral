
Description
-----------

Viral Discount allows admins to specify referral discounts for particular products.  After purchase, a user is given the option to refer friends to purchase the product. For every friend who purchases the product, the user's order total is decreased by a pre-set amount.


Installation
------------

* Follow the general directions available at: 
http://drupal.org/node/70151

* Set your payment method to authorization only, uc_viral only works with delayed collection only previously authorized payments

* Edit the product for which you would like to add a viral discount.  Select 'Enabled' under Viral Discount and enter the discount amount.  Optionally enter a redirect link if you want referred users to be sent to a page other than the product page.

